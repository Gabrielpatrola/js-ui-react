import Grid from "@mui/material/Grid";
import BitcoinCard from "./Card";
import { getBitcoinPrice } from "../../services/bitcoin";
import { useState, useEffect } from "react";
import CurrencyExchangeResponse from "../../interfaces/Bitcoin/currencyExchangeResponse";

export default function BitcoinCurrencyExchange() {
  const [data, setData] = useState<CurrencyExchangeResponse>();

  const getData = async () => {
    const response = await getBitcoinPrice();
    setData(response);
  }

  useEffect(() => {
    getData()
  }, []);

  return (
    <Grid container spacing={3}>
      {data?.currencies &&
        Object.values(data.currencies).map((currency) => (
          <Grid item xs={12} md={4} key={currency.code}>
            <BitcoinCard
              disclaimer={data?.disclaimer}
              updatedDate={data?.updatedDate}
              code={currency.code}
              description={currency.description}
              rate={currency.rate}
            />
          </Grid>
        ))}
    </Grid>
  );
}
