import React, { useState, useEffect, Fragment } from "react";
import {
  DataGrid,
  GridColDef,
  GridValueFormatterParams,
} from "@mui/x-data-grid";
import Title from "../Template/Title";
import { getDogFacts } from "../../services/dogFact";
import DogFactsResponse from "../../interfaces/DogFacts/dogFactsResponse";
import { Box } from "@mui/material";

const columns: GridColDef[] = [
  { field: "id", headerName: "ID", width: 90 },
  {
    field: "fact",
    headerName: "Fact",
    flex: 1,
  },
  {
    field: "created_at",
    headerName: "Created At",
    flex: 1,
    valueFormatter: (params: GridValueFormatterParams<string>) => {
      return new Date(params.value).toDateString();
    },
  },
  {
    field: "updated_at",
    headerName: "Updated At",
    flex: 1,
    valueFormatter: (params: GridValueFormatterParams<string>) => {
      return new Date(params.value).toDateString();
    },
  },
];

export default function DogFacts() {
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(25);
  const [dogFacts, setDogFacts] = useState<DogFactsResponse>();

  const getData = async () => {
    const response = await getDogFacts(currentPage, pageSize);

    setDogFacts(response);
  }

  useEffect(() => {
    getData()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentPage, pageSize]);

  return (
    <Fragment>
      <Title>Dog Facts</Title>
      {dogFacts?.data && (
        <Box sx={{ height: 400, width: "100%" }}>
          <DataGrid
            rows={dogFacts.data}
            rowCount={dogFacts.total}
            columns={columns}
            pageSizeOptions={[25,50,100]}
            pagination
            paginationModel={{
              page: Number(dogFacts.current_page),
              pageSize: Number(dogFacts.per_page),
            }}
            paginationMode="server"
            onPaginationModelChange={(params) => {
              if (params.pageSize !== pageSize) {
                setPageSize(params.pageSize)
              }
              if (params.page !== currentPage) {
                setCurrentPage(params.page)
              }
            }}
          />
        </Box>
      )}
    </Fragment>
  );
}
