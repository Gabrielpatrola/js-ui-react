import React from 'react';
import { render, screen } from '@testing-library/react';
import Orders from '../components/Orders';
import Deposits from '../components/Deposits';
import BitcoinCard from '../components/Bitcoin/Card';

test('renders orders', () => {
  render(<Orders />);
  const title = screen.getByText(/recent orders/i);
  expect(title).toBeInTheDocument();
});

test('renders deposits', () => {
  render(<Deposits />);
  const title = screen.getByText(/recent deposits/i);

  expect(title).toBeInTheDocument();
});

test('renders currency', () => {
  const data = {
    title: "Test",
    disclaimer: "Disclaimer",
    updatedDate: "2023-08-07T23:33:00+00:00",
    code: "USD",
    description: "United States Dollar",
    rate: "29,201.4147",
  };

  render(<BitcoinCard title={data.title}
    disclaimer={data.disclaimer}
    updatedDate={data.updatedDate}
    code={data.code}
    description={data.description}
    rate={data.rate}
  />);

  const timeElement = screen.getByText(/2023-08-07T23:33:00\+00:00/);
  expect(timeElement).toBeInTheDocument();

  const titleElement = screen.getByText(/Test/);
  expect(titleElement).toBeInTheDocument();

  const rateElement = screen.getByText(/29,201.41/);
  expect(rateElement).toBeInTheDocument();

  const disclaimerElement = screen.getByText(/Disclaimer/);
  expect(disclaimerElement).toBeInTheDocument();
});
