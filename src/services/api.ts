import axios from "axios";

const coinApi = axios.create({
  baseURL: process.env.REACT_APP_COIN_API_URL,
});

const animalsApi = axios.create({
  baseURL: process.env.REACT_APP_ANIMALS_API_URL,
});


export { coinApi, animalsApi };
