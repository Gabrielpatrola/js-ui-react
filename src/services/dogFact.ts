import { animalsApi } from "./api";
import DogFactsResponse from "../interfaces/DogFacts/dogFactsResponse";

async function getDogFacts(currentPage: number, pageSize: number): Promise<DogFactsResponse> {
  const { data } = await animalsApi.get(`dog-facts?page=${currentPage}&pageSize=${pageSize}`);

  return data;
}

export { getDogFacts };
