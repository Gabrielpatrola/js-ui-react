import CurrencyExchangeResponse from "../interfaces/Bitcoin/currencyExchangeResponse";
import { coinApi } from "./api";

async function getBitcoinPrice(): Promise<CurrencyExchangeResponse> {
  const {
    data: { chartName, disclaimer, time, bpi },
  } = await coinApi.get("currentprice.json");

  return {
    chartName,
    disclaimer,
    updatedDate: time.updated,
    currencies: bpi,
  };
}

export { getBitcoinPrice };
