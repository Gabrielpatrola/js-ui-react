export default interface Currency {
  code: string;
  description: string;
  rate: string;
}
