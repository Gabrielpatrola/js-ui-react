import Currency from "./currency";

export default interface CurrencyExchangeResponse {
  chartName: string;
  disclaimer: string;
  updatedDate: string;
  currencies: { [key: string]: Currency };
}
