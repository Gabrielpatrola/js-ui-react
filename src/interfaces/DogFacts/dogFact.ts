export default interface DogFact {
  id: number;
  fact: string;
  created_at: string;
  updated_at: string;
}
