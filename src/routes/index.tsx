import { Navigate, Route, Routes } from "react-router-dom";
import Dashboard from "../components/Dashboard";
import Bitcoin from "../components/Bitcoin";
import RoutePath from "../constants/routePath";

export default function AppRoutes() {
  return (
    <Routes>
      <Route path="/" element={<Navigate replace to={RoutePath.DASHBOARD} />} />
      <Route path={RoutePath.DASHBOARD} element={<Dashboard />} />
      <Route path={RoutePath.BITCOIN} element={<Bitcoin />} />
    </Routes>
  );
}
