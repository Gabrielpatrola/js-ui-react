enum RoutePath {
  DASHBOARD = "dashboard",
  BITCOIN = "bitcoin",
  ORDERS = "orders",
  CUSTOMERS = "customers",
  REPORTS = "reports",
  INTEGRATIONS = "integrations",
}

export default RoutePath;
