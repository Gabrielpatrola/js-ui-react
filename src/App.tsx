import React from "react";
import "./styles/App.css";
import Routes from "./routes";
import { ThemeProvider, createTheme } from "@mui/material";
import Template from "./components/Template";

const mdTheme = createTheme();

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={mdTheme}>
        <Template>
          <Routes />
        </Template>
      </ThemeProvider>
    </div>
  );
}

export default App;
